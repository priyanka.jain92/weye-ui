import React from "react";
import { storiesOf } from "@storybook/react";
import Button from "./index";

storiesOf("Button", module).add("Default", () => (
  <Button
    onClick={() => {
      alert("I am a button! =)");
    }}
  >
    Click me
  </Button>
));
