const pkg = require("./package.json");
const rules = require("./configs/rules.config");

module.exports = {
  module: {
    rules: rules,
  },
  entry: "./src/index.js",
  output: {
    filename: pkg.main,
    library: "",
    libraryTarget: "commonjs",
  },
  resolve: {
    extensions: [".js", ".jsx", ".json"],
    modules: ["node_modules"],
  },
};
